from django.db import models

# Create your models here.
# Create your models here.
class Author(models.Model):

    first_name = models.CharField(max_length=35, null=False)
    last_name = models.CharField(max_length=35, null=False)
    age = models.PositiveIntegerField(default=None, null=True)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)

    class Meta:
        ordering = ['pk']