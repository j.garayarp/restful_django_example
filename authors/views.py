# Django rest framework
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
# Models 
from authors.models import Author

# Create your views here.
@api_view(['GET', 'POST'])
def authors(request):
    if request.method == 'POST':
        first_name = request.data['first_name']
        last_name = request.data['last_name']
        age = request.data.get('age', None)
        author = Author.objects.create(
            first_name=first_name,
            last_name=last_name,
            age=age
        )
        data = {
            'first_name': author.first_name,
            'last_name': author.last_name,
            'age': author.age,
        }
        return Response(data, status=status.HTTP_201_CREATED)
    authors = Author.objects.order_by('last_name').all()
    data = []
    for author in authors:
        data.append({
            'id': author.pk,
            'apellido': author.last_name
        })
    return Response(data, status=status.HTTP_200_OK)


@api_view(['GET'])
def authors_id(request, **kwargs):
    id_author = kwargs['id_author']
    author_data = Author.objects.filter(pk = id_author).first()
    data = {
        'id': author_data.pk,
        'first_name': author_data.first_name,
        'last_name': author_data.last_name,
        'age': author_data.age,
        'created': author_data.created,
        'modified': author_data.modified,
    }
    return Response(data, status=status.HTTP_200_OK)