# Django
from django.contrib import admin
# Models
from authors.models import Author

# Register your models here.
@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):

    list_display = ('pk', 'first_name', 'last_name', 'age')

    search_fields = (
        'first_name',
        'last_name'
    )

    readonly_fields = ('created', 'modified')