# Django
from django.contrib import admin
# Models
from categories.models import Category

# Register your models here.
@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):

    list_display = ('pk', 'category')

    search_fields = (
        'category',
    )

    readonly_fields = ('created', 'modified')