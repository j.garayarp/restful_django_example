# Django rest framework
from rest_framework.decorators import api_view
from rest_framework.response import Response
# Models 
from categories.models import Category

# Create your views here.
@api_view(['GET'])
def list_categories(request):
    categories = Category.objects.all()
    data = []
    for category in categories:
        data.append({
            'id': category.pk,
            'category': category.category,
        })
    return Response(data)