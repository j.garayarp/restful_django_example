# Django rest framework
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
# Models 
from books.models import Books
from authors.models import Author
from categories.models import Category

# Create your views here.
@api_view(['GET', 'POST'])
def books(request):
    if request.method == 'POST':
        title = request.data['title']
        editorial = request.data['editorial']
        id_author = request.data['id_author']
        author = Author.objects.get(pk = id_author)
        id_category = request.data['id_category']
        category = Category.objects.get(pk = id_category)
        book = Books.objects.create(
            title=title,
            editorial=editorial,
            author=author,
            category=category,
        )
        data = {
            'id': book.pk,
            'title': book.title,
            'editorial': book.editorial,
            'author': book.author.first_name,
            'category': book.category.category,
        }
        return Response(data, status=status.HTTP_201_CREATED)
    
    books = Books.objects.all()
    data = []
    for book in books:
        data.append({
            'id': book.pk,
            'title': book.title,
            'category': book.category.category,
            'author': book.author.first_name + ' ' + book.author.last_name
        })
    return Response(data)

@api_view(['GET', 'PUT', 'DELETE'])
def books_id(request, **kwargs):
    id_book = kwargs['id_book']
    if request.method == 'PUT':
        title = request.data['title']
        editorial = request.data['editorial']
        id_author = request.data['id_author']
        author = Author.objects.get(pk = id_author)
        id_category = request.data['id_category']
        category = Category.objects.get(pk = id_category)
        book = Books.objects.get(pk = id_book)
        book.title = title
        book.editorial = editorial
        book.author = author
        book.category = category
        book.save()
        data = {
            'id': book.pk,
            'title': book.title,
            'editorial': book.editorial,
            'author': book.author.first_name,
            'category': book.category.category,
        }
        return Response(data, status=status.HTTP_202_ACCEPTED)
    if request.method == 'DELETE':
        book = Books.objects.get(pk = id_book)
        data = {
            'id': book.pk,
            'author': book.author.first_name + ' ' + book.author.last_name,
            'category': book.category.category,
            'created': book.created,
            'modified': book.modified,
        }
        Books.objects.filter(pk = id_book).delete()
        return Response(data, status=status.HTTP_202_ACCEPTED)
    # GET
    book_data = Books.objects.filter(pk = id_book).first()
    data = {
        'id': book_data.pk,
        'author': book_data.author.first_name + ' ' + book_data.author.last_name,
        'category': book_data.category.category,
        'created': book_data.created,
        'modified': book_data.modified,
    }
    return Response(data, status=status.HTTP_200_OK)