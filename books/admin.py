# Django
from django.contrib import admin
# Models
from books.models import Books

# Register your models here.
@admin.register(Books)
class BookAdmin(admin.ModelAdmin):

    list_display = ('pk', 'title', 'editorial', 'author', 'category')

    search_fields = (
        'title',
        'editorial'
    )

    readonly_fields = ('created', 'modified')