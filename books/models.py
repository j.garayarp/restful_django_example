from django.db import models
# Models
from authors.models import Author
from categories.models import Category

# Create your models here.
class Books(models.Model):

    title = models.CharField(max_length=255, null=False)
    editorial = models.CharField(max_length=255, null=True, default=None)

    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.title)

    class Meta:
        ordering = ['pk']