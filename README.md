# A restful Django API
I only use function based views (django-rest-framework) for this challenge.
## Models
Categories (id, category)
Authors (id, first_name, last_name, age(optional))
Books (title, editorial, author(fk), category(fk))
## Functionalities
- List categories
- Create author
- List authors (order by last_name, get id, last_name)
- Author detail (all data)
- Create book
- Book detail (get author's first_name, last_name and category name)
- List books (get id, title, category, author's first_name and last_name)
- Delete book
- Update book